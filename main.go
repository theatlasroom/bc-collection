package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
)

const (
	baseURL  = "https://bandcamp.com"
	username = "theatlasroom"
)

// Always set a timeout on http reqs
var client = &http.Client{
	Timeout: time.Second * 10,
}

func handleError(err error) {
	// TODO: need a logger / handler
	if err != nil {
		log.Fatal(err)
	}
}

func execute(url string) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPost, url, nil)
	handleError(err)

	// -H 'Content-Type: application/x-www-form-urlencoded'
	// -H 'X-Requested-With: XMLHttpRequest'
	// -H 'Origin: https://bandcamp.com'
	// -H 'Referer: https://bandcamp.com/theatlasroom'
	// req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Origin", "https://bandcamp.com")
	req.Header.Set("Referer", "https://bandcamp.com/theatlasroom")
	resp, err := client.Do(req)
	handleError(err)

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

func fetchProfile(baseURL, username string) {
	url := strings.Join([]string{baseURL, username}, "/")
	c := colly.NewCollector()

	// Find and visit all links
	c.OnHTML("a", func(e *colly.HTMLElement) {
		e.Request.Visit(e.Attr("href"))
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.Visit(url)
}

func main() {
	fmt.Printf("Fetching collection for %v\n", username)
	// https://bandcamp.com/api/fancollection/1/collection_items
	fetchProfile(baseURL, username)
	// req := client.NewRequest(http.MethodPost, url)
}
